# Descritpion

Container image for samba server with one user configurable through
environment variables.

Mount your shared dirctory to /mnt in the container.

# Build

```
podman build . -t quay.io/tbalsys/samba:4.13.8-r0
```

# Run

```
podman run -d --init \
  --name smbd \
  -e USERNAME=admin \
  -e PASSWORD=admin \
  -p 445:445 \
  -v .:/mnt:z \
  quay.io/tbalsys/samba:4.13.8-r0
```
