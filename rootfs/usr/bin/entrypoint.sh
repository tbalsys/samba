#!/bin/sh

function setConf {
	KEY="$1"
	VALUE="$2"
	FILE="$3"
	echo "$FILE: $KEY=$VALUE"
	ESCAPED=$(printf '%s\n' "$VALUE" | sed -e 's/[\/&]/\\&/g')
	sed -i "1,/^#\?\s*$KEY\s*=.*\$/s/^#\?\s*$KEY\s*=.*\$/$KEY=$ESCAPED/" $FILE
}

if [ -n "$USERNAME" ]; then
  if ! grep -q $USERNAME /etc/passwd; then
    setConf "valid users" "$USERNAME" /etc/samba/smb.conf
    adduser -s /sbin/nologin -h /home/samba -G users -H -D $USERNAME
    chown $USERNAME /mnt

    if [ -n "$PASSWORD" ]; then
      printf "$PASSWORD\n$PASSWORD\n" | smbpasswd -a $USERNAME
    fi
  fi
fi

ls /etc/samba/conf.d/* | sed -e 's/^/include = /' >> /etc/samba/smb.conf

exec smbd --foreground --log-stdout --no-process-group
