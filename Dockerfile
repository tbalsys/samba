FROM alpine:3.13

RUN apk add --no-cache \
    samba-common-tools \
    samba-client \
    samba-server

EXPOSE 445/tcp

CMD ["entrypoint.sh"]

COPY rootfs /